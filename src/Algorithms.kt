/**
 * @author Rychagov D.I. on 18.11.2017.
 */

fun encrypt(plain: String, rawKey: String) {
  var result = plain
  val key = calculateKey(rawKey)

  repeat(key) {
    result = encryptionStep(result, key)
  }

  println("\nResult:\n$result")
}

fun decrypt(cipher: String, rawKey: String) {
  var result = cipher
  val key = calculateKey(rawKey)

  repeat(key) {
    result = decryptionStep(result, key)
  }

  println("\nResult:\n$result")
}

private fun encryptionStep(plain: String, key: Int): String {
  return plain
      .mapIndexed { position, symbol -> symbol.plus(position.inc().mod(key)) }
      .fold("", String::plus)
}

private fun decryptionStep(cipher: String, key: Int): String {
  return cipher
      .mapIndexed { position, symbol -> symbol.minus(position.inc().mod(key)) }
      .fold("", String::plus)
}

private fun calculateKey(rawKey: String): Int {
  return rawKey
      .fold(0) { sum, symbol -> sum + symbol.toInt() }
      .div(Math.pow(rawKey.length.toDouble(), 1.5))
      .toInt()
}