import java.util.*

/**
 * @author Rychagov D.I. on 18.11.2017.
 */

class Initializer {

  companion object {
    private const val MESSAGE_CHOOSE_ACTION = "\nChoose your action:"
    private const val MESSAGE_WRONG_INPUT = "\nWrong input!"
    private const val MESSAGE_ERROR = "\nOops! Some errors were occurred :( Please, try again"
    private const val MESSAGE_WRITE_TEXT = "\nWrite your text here:"
    private const val MESSAGE_WRITE_KEY = "\nWrite key here:"

    private const val ACTION_ENCRYPT = "1. Encrypt message"
    private const val ACTION_DECRYPT = "2. Decrypt message"
    private const val ACTION_CLOSE = "0. Close program"
  }

  fun init() {
    when(choseAction()) {
      ACTION_ENCRYPT -> encrypt(readInput(), readKey())
      ACTION_DECRYPT -> decrypt(readInput(), readKey())
      ACTION_CLOSE -> System.exit(0)
    }

    init()
  }

  private fun choseAction(): String {
    println(MESSAGE_CHOOSE_ACTION)

    println(ACTION_ENCRYPT)
    println(ACTION_DECRYPT)
    println(ACTION_CLOSE)

    val scanner = Scanner(System.`in`)

    return try {
      when (scanner.nextInt()) {
        1 -> ACTION_ENCRYPT
        2 -> ACTION_DECRYPT
        0 -> ACTION_CLOSE
        else -> throw InputMismatchException()
      }
    } catch (e: InputMismatchException) {
      println(MESSAGE_WRONG_INPUT)
      choseAction()
    }
  }

  private fun readInput(): String {
    println(MESSAGE_WRITE_TEXT)

    return readLine() ?: ""
  }

  private fun readKey(): String {
    println(MESSAGE_WRITE_KEY)

    return readLine().notEmptyOrNull() ?: readKey()
  }
}