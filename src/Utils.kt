/**
 * @author Rychagov D.I. on 18.11.2017.
 */
 
fun String?.notEmptyOrNull() = if (this != null && this.isEmpty()) null else this

